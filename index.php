<?php

	/*
	
	Query for all aged message forums based off the tbl_layer.date_to_decay values. Note: most
	forums will have a NULL entry here, which means they don't age and should be ignored.
	This will be called hourly on a regular cronjob.
	
	0 * * * *       /usr/bin/php /your_server_path/api/plugins/remove_aged/index.php
	
	1. Loop through each forum, query for all messages from that forum.
	2. Loop through each message from that forum and search individually within that message for
	images that are held on this server. They will start with the config.json JSON entry uploads.vendor.imageURL
	if images are uploaded via AmazonAWS (/digitalocean), or with the current server URL /images/im/ if uploaded
	to the same server.
	3. Delete the image from using the AmazonAWS API or the local file-system unlink();
	4. Delete the message
	5. Repeat for all messages in forum. 
	6. Delete the forum.
	
	*/

	$preview = false;		//Usually set to false, unless we are testing this manually 

	use Aws\S3\S3Client;
	use Aws\S3\Exception\S3Exception;
	
	require('vendor/aws-autoloader.php');


	function trim_trailing_slash_local($str) {
        return rtrim($str, "/");
    }
    
    function add_trailing_slash_local($str) {
        //Remove and then add
        return rtrim($str, "/") . '/';
    }
    
    function was_created_on_group($api, $image_file, $image_folder, $this_layer) {
    	//Returns true for on this group, or false for not on this group
    	$sql = "SELECT int_layer_id FROM tbl_media WHERE var_filename = ?";
    	$result_msgs = $api->db_select($sql, [["s", $image_file]]);
		if($row = $api->db_fetch_array($result_msgs))
		{
			if($row['int_layer_id'] == $this_layer) {
				//Yes, the image was created on this layer
				return true;
			}
		
		}
		
		return false;
    }
    
    function delete_media_entry($api, $image_file) {
    	//Also fully delete the media entry in the database, so that we will not grow disk space continuously (note: we can up to a max 1M folders created on a disk /0/1/2/3/4/5
    	//10 to the power of 6 = 1M, but at least that has a maximum - I don't think we need to delete these individually as we go? Although monitor the size of the drive used by these). The answer would be to delete these in this function if the folder was empty.
    	$sql = "DELETE FROM tbl_media WHERE var_filename = ?";
    	$result = $api->db_select($sql, [["s", $image_file]]);
    	return;
    }
    
    
	function delete_image($image_file, $image_folder, $preview = false) {
		global $local_server_path;
		global $cnf;
		global $aged_config;
		
		

		if(isset($cnf['uploads']['use'])) {
			if($cnf['uploads']['use'] == "amazonAWS") {

				if(isset($cnf['uploads']['vendor']['amazonAWS']['uploadUseSSL'])) {
					$use_ssl = $cnf['uploads']['vendor']['amazonAWS']['uploadUseSSL'];
					
				} else {
					$use_ssl = false;		//Default
				}
				
				if(isset($cnf['uploads']['vendor']['amazonAWS']['uploadEndPoint'])) {
					$endpoint = $cnf['uploads']['vendor']['amazonAWS']['uploadEndPoint'];
				} else {
					$endpoint = "https://s3.amazonaws.com";		//Default
				}
				
				if(isset($cnf['uploads']['vendor']['amazonAWS']['bucket'])) {
					$bucket = $cnf['uploads']['vendor']['amazonAWS']['bucket'];
				} else {
					$bucket = "ajmp";		//Default
				}
				
				if(isset($cnf['uploads']['vendor']['amazonAWS']['region'])) {
					$region = $cnf['uploads']['vendor']['amazonAWS']['region'];
				} else {
					$region = 'nyc3';
				}		
		
				
				
				$output = "Preparing to delete image: " . $image_file . "    from bucket: " . $bucket .   "   from region: " . $region .  "   at endpoint: " . $endpoint;
				echo $output . "\n";
				error_log($output);
				
				
				if($preview !== false) {
					//A preview, always return deleted
					return true;
				} else {
		
					echo "S3 connection about to be made\n";
										
					//Get an S3 client
					$s3 = new Aws\S3\S3Client([
							'version' => 'latest',
							'region'  => $region,				
							'endpoint' => $endpoint,			//E.g. 'https://nyc3.digitaloceanspaces.com'
							'credentials' => [
									'key'    => $cnf['uploads']['vendor']['amazonAWS']['accessKey'],
									'secret' => $cnf['uploads']['vendor']['amazonAWS']['secretKey'],
								]
					]);
					
					
		
					if($s3 != false) {
						echo "S3 connection made\n";
						
						try {
							// Upload data.
							$s3->deleteObject([
								'Bucket' => $bucket,
								'Key'    => $image_file
							]);

							// Print the URL to the object.
							error_log("Successfully deleted: " . $image_file);
							echo "Successfully deleted: " . $image_file . "\n";
							//Deleted correctly
						
							return true;
						} catch (S3Exception $e) {
							//Error deleting from Amazon
							error_log($e->getMessage());
							echo "Error deleting: " . $e->getMessage() . "\n";
							return false;
						}
					} else {
						echo "S3 connection not made\n";
						return false;
					}
				} 
			} else {
			
				//Delete locally
				$output = "Preparing to delete image: " . $image_folder . $image_file;
				
				
				
				//Delete our own one, just in case it is not already deleted
				echo $output . "\n";
				error_log($output);
				if($preview !== true) {
					$file_to_delete = $image_folder . $image_file;
					if(file_exists($file_to_delete)) {
						if(unlink($file_to_delete)) {
							echo "Success deleting " . $file_to_delete . ".\n";
							error_log("Success deleting " . $file_to_delete . "  [error log version]");
						} else {
							echo "Failure deleting " . $file_to_delete . ".\n";
							error_log("Failure deleting " . $file_to_delete . "  [error log version]");
						}
					} else {
						echo "Note: File has already been deleted: " . $file_to_delete . ".\n";
						error_log("Note: File has already been deleted: " . $file_to_delete . "  [error log version]");
					}
				}
				
				//Now do this for each of the $cnf['ips'], call a delete_image script on each 
				if(isset($aged_config['urlPathToDeleteScript'])) {
					for($cnt = 0; $cnt < count($cnf['ips']); $cnt++) {
						$url = "http://" . $cnf['ips'][$cnt] . $aged_config['urlPathToDeleteScript'] . "?code=" . $aged_config['securityCode'] . "&imageName=" . $image_file;
						echo "Running URL " . $url . " to delete image.\n";
						if($preview !== true) {
							file_get_contents($url);
						}
					}
				}
				
			}
		}

		
		
	}    
    


	if(!isset($aged_config)) {
        //Get global plugin config - but only once
		$data = file_get_contents (dirname(__FILE__) . "/config/config.json");
        if($data) {
            $aged_config = json_decode($data, true);
            if(!isset($aged_config)) {
                echo "Error: remove_aged config/config.json is not valid JSON.";
                exit(0);
            }
     
        } else {
            echo "Error: Missing config/config.json in remove_aged plugin.";
            exit(0);
     
        }
  
  
    }



    $agent = $aged_config['agent'];
	ini_set("user_agent",$agent);
	$_SERVER['HTTP_USER_AGENT'] = $agent;
	$start_path = add_trailing_slash_local($aged_config['serverPath']);
	if($aged_config['layerTitleDbOverride']) {
		//Override the selected database - this should be used on secondary servers (e.g. in atomjump.com's case)
		//to delete them off a particular sharded database, not the main one. E.g. "api0", "api1", which are stored in the config.json
		//file for the main server under the 'scaleUp' options
		$_REQUEST['uniqueFeedbackId'] = $aged_config['layerTitleDbOverride'];
	}
	if($aged_config['preview']) {
		$preview = $aged_config['preview'];
	}
	
	if($aged_config['randomPause']) {
		//Pause execution for a random interval to prevent multiple servers in a cluster calling at the same time on the cron.
		$pause_by = rand(1,$aged_config['randomPause']);
		sleep($pause_by);
	}

	$image_folder = $start_path . "images/im/";
	
	$notify = false;
	include_once($start_path . 'config/db_connect.php');	
	
	$define_classes_path = $start_path;     //This flag ensures we have access to the typical classes, before the cls.pluginapi.php is included
	require($start_path . "classes/cls.pluginapi.php");
	
	$api = new cls_plugin_api();
	
	
	if($preview == true) {
		echo "Preview mode ON\n";
	}
	
	echo "Using database host: " .  $db_cnf['hosts'][0] . "  name:" . $db_cnf['name'] . "\n";
		
	$delete_forum = false;		
	if(isset($db_cnf['deleteDeletes'])) {
		//Defaults to the server-defined option, unless..
		$delete_forum = $db_cnf['deleteDeletes'];
	}
	if(isset($aged_config['deleteForum'])) {
		//Unless we have an override in our local config
		$delete_forum = $aged_config['deleteForum'];	
	}

	echo "Checking for decayed layers...\n";
	$sql = "SELECT * FROM tbl_layer WHERE date_to_decay IS NOT NULL AND date_to_decay < NOW()";
    $result = $api->db_select($sql);
	while($row = $api->db_fetch_array($result))
	{
			$this_layer = $row['int_layer_id'];
			
			echo "Layer: " . $this_layer . "\n";
			
			$sql = "SELECT int_ssshout_id, var_shouted, var_shouted_processed FROM tbl_ssshout WHERE int_layer_id = ?";
			$result_msgs = $api->db_select($sql, [["i", $this_layer]]);
			while($row_msg = $api->db_fetch_array($result_msgs))
			{
				echo "Message: " . $row_msg['var_shouted'] . "    ID:" . $row_msg['int_ssshout_id'] . "\n";
				
				global $cnf;
			
				if($delete_forum == true) {
					
					
					//Search for any images in the message
					echo "Search term = " . $cnf['uploads']['replaceHiResURLMatch'] . "\n";
					$url_matching = "ajmp";		//Works with Amazon based jpgs on atomjump.com which include ajmp.
					if($cnf['uploads']['replaceHiResURLMatch']) $url_matching = $cnf['uploads']['replaceHiResURLMatch'];
					
					$ext_types = array(".jpg", ".mp3", ".mp4", ".pdf");
					for($cnt_ext = 0; $cnt_ext < count($ext_types); $cnt_ext ++) {
				
						$ext = $ext_types[$cnt_ext];
					
						$preg_search = "/.*?" . $url_matching ."(.*?)\\" . $ext . "/i";
						preg_match_all($preg_search, $row_msg['var_shouted_processed'], $matches);		//Need to match against the full processed message - it may include a 
																										//thumbnail .jpg and .mp4, for example for videos.
					
						
							
						if(count($matches) > 1) {
							//Yes we have at least one image
							for($cnt = 0; $cnt < count($matches[1]); $cnt++) {
								echo "Matched image raw: " . $matches[1][$cnt] . "\n";
								list($end_of_url, $raw_image_name) = explode( "/images/im/", $matches[1][$cnt]);
								
								$image_name = $raw_image_name . $ext;
								if($ext == ".jpg") {
									$image_hi_name = $raw_image_name . "_HI.jpg";
								} else {
									$image_hi_name = null;
								}
								echo "Image name: " . $image_name . "\n";
								
								//Check if this image was created from this group
								$from_this_group = was_created_on_group($api, $image_name, $image_folder, $this_layer);
					
								if($from_this_group == true) {
								
									//Delete this image
									delete_image($image_name, $image_folder, $preview);
									if($ext == ".jpg") {
										//Delete the hi-res version
										delete_image($image_hi_name, $image_folder, $preview);
									}
								
									delete_media_entry($api, $image_name);		//Since we're deleting the core entry, and not just the hi-res version, we also need
																				//to remove the database version of this - note: once this has gone we have no way to trace it 
																				//again.
								
								}
					
								
							}
						}
					}
						
					
					//Delete the record
					if($preview == false) {
						echo "Deleting message " . $row_msg['int_ssshout_id'] . "\n";
						error_log("Deleting message " . $row_msg['int_ssshout_id']);
						$sql_del = "DELETE FROM tbl_ssshout WHERE int_ssshout_id = ?";
						$api->db_select($sql_del, [["i", $row_msg['int_ssshout_id']]]);
					}
				
				
				} else {
					echo "Deactivating. But leaving images.";
					if($preview == false) {
					   echo "Deactivating message " . $row_msg['int_ssshout_id'] . "\n";
					   error_log("Deactivating message " . $row_msg['int_ssshout_id']);
					   
					   $api->db_update("tbl_ssshout", "enm_active = 'false' WHERE int_ssshout_id = ?", [["i", $row_msg['int_ssshout_id']]]);
					}
				}
			}
			
			global $cnf;
			
			if($delete_forum === true) {
				//Now delete the layer itself
				if($preview == false) {
					error_log("Deleting layer " . $this_layer);
					echo "Deleting layer " . $this_layer . "\n";
					$sql_del = "DELETE FROM tbl_layer WHERE int_layer_id = ?";
					$api->db_select($sql_del, [["i", $this_layer]]);
				} else {
					echo "Would be deleting layer " . $this_layer . "\n";
				}
			} else {
				echo "Not deleting layer " . $this_layer . "\n";
				
				if($preview == false) {
					//But since we have now processed this layer and deactivated it's messages, we don't need to again.
					$api->db_update("tbl_layer", "date_to_decay = NULL WHERE int_layer_id = ?", [["i", $this_layer]]);
					echo "Cleared layer " . $this_layer . " decay date.\n";
				} else {
					echo "Would be clearing layer " . $this_layer . " decay date.\n";
				}
			}
		
	} 
		

	
	session_destroy();  //remove session




?>
